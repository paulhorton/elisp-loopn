;;; loopn.el --- loon n times  -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231009
;; Updated: 20231009
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:


(defmacro loopn (n &rest body)
  "Eval BODY N times.

Return value:
  if 'cl-return used   whatever was thrown
  Otherwise  value of last body expression evaluated
  Finally nil, if N is zero.

Type of N
Logically N should be a non-negative integer.
But currently floats are quietly
accepted with the effect of rounding them up.
"
  (declare
   (indent 1)
   (debug (form body))
   )
  (let ((countdown  (gensym "countdown"))
        (result     (gensym "result"))
        )
    `(let (
           ;; Evaluates N to compute the number of loop interations.
           ;; this may have side-effects even when N is malformed.
           ;; For example:  (loopn (insert "frog"))
           ;;
           ;; dotimes also suffers from this.
           ;; For example:  (dotimes (_ (insert "frog")))
           (,countdown ,n)
           ,result
           )
       ;; We check the type of COUNTDOWN not N,
       ;; to avoid evaluating N more than once.
       (cl-check-type ,countdown number "loopn arg1.")
       (or
        ; unless body throws a cl-return,
        ; (catch (while...)) will eval to nil.
        (catch '--cl-block-nil--
          (while (< 0 ,countdown)
            (setq ,result
                  (progn
                    (setq ,countdown (1- ,countdown))
                    ,@body
                    ))))
        ,result
        ))))



(provide 'loopn)

;;; loopn.el ends here
