Simple macro to loop n times.

Similar to `(dotimes (_ 0 n))'
but with potentially useful return value.
cl-return is supported for early exit
and otherwise the last body expression evaluated
is returned.

See also: dotimes, cl-dotimes, dotimes*, while, until
