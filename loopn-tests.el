;;; loopn-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231009
;; Updated: 20231009
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest loopn/fallthrough ()
  "loopn return values via fall-through"
  (should
   (eq
    nil
    (loopn 0 t)
    ))
   (should
    (eq
     t
     (loopn 3 t)
     ))
   (should
    (eq
     'thecharm
     (loopn 3
       'this
       'that
       'thecharm
     ))
    ))


(ert-deftest loopn/simple ()
  (should
   (=
    10
    (let ((x 0))
      (loopn 5
        (cl-incf x 2)
        )
      x
      ))))


(ert-deftest loopn/early-return ()
  (should
   (=
    6
    (let ((x 0))
      (loopn 5
        (cl-incf x 2)
        (when (< 5 x)
          (cl-return x)
          )
        x
        ))))
  (should
   (=
    10
    (let ((x 0))
      (loopn 5
        (cl-incf x 2)
        x
        ))))
   )


;;; loopn-tests.el ends here
